# Roblox Material Executor

A material.io inspired executor that's made using the MaterialR+ library.

![](https://cdn.discordapp.com/attachments/814978302535139403/865306775925162044/2021-07-15_20-59-30.mp4)

# Importing

Download MEx from the `/source/` directory and sideload it into Roblox Studio.
You can **only** import MEx via the server by running `require(path.to.mex):Load(PlayerInstance)`. Please note that MEx can only be imported **once** unless changes are applied to file `MainModule.lua` --> Remove line 9 & 10.

## License

This release of MEx is licensed with the **GNU General Public License 3.0**. (GPLv3)

## Credits

- Creator of MaterialR+ `fivefactor`.
- Creator of Adonis Loadstring / Rerubi.
- material.io Design Language.
