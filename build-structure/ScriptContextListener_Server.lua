--// Import the ScriptContext Service to log for errors
local ScriptContext = game:GetService("ScriptContext")

--// Track the Server_RunService script
local ScriptToTrack = script.Parent:WaitForChild("Server_RunService")

--// On error, send the client information that execution failed
ScriptContext.Error:Connect(function(ErrorMessage, Trace, Script)
	if Script == ScriptToTrack then
		script.Parent.Execute:FireClient(game:GetService("Players"):FindFirstChild(script.Parent.Parent.Parent.Name), string.format("Error!\n%s", ErrorMessage), true)
	end
end)
