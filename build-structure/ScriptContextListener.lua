local ScriptContext = game:GetService("ScriptContext")

local ScriptToTrack = script.Parent:WaitForChild("UserInterface_Manager")

local function notification(txt)

	local Obj = script.SnackbarAlert:Clone()
	Obj.Name = "Snackbar_Alert"

	Obj.Text.Text = txt

	Obj.Parent = script.Parent.Backdrop

	Obj.Tween.Disabled = false

	wait(4)

	if script.Parent:FindFirstChild("UserInterface_Manager") then

		script.Parent.UserInterface_Manager:Destroy()

	end

	local Backup = script:WaitForChild("UserInterface_Manager"):Clone()

	Backup.Parent = script.Parent
	Backup.Disabled = false

end

game:GetService("LogService").MessageOut:Connect(function(message, Type)
    if Type == Enum.MessageType.MessageError then
        local msg = message:match("UserInterface_Manager")
        if msg then
            notification("Error in main thread!\nAttempting to rebuild script base in three seconds...")
        end
    end
end)

--ScriptContext.Error:Connect(function(ErrorMessage, Trace, Script)
--   if Script == ScriptToTrack then
--      notification("Error in main thread!\nAttempting to rebuild script base in three seconds...")
--   end
--end)

repeat wait() until (script.Parent.UserInterface_Manager)

wait(1)

local UIM = script.Parent.UserInterface_Manager:Clone()
UIM.Disabled = true
UIM.Parent = script

local BackupIndicator = Instance.new("StringValue")
BackupIndicator.Name = "BackupScript"
BackupIndicator.Parent = UIM
