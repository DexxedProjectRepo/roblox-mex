--// The module used for importing
local module = {}

--// Function :Load initializes on argument Player
function module:Load(target)
	_G.target = target
	local target = game.Players:WaitForChild(_G.target)
	script.MEx:Clone().Parent = target.PlayerGui
	wait(0.1)
	script:Destroy()
end

--// Return after finishing
return module
