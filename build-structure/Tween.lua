local function tween(obj, info, goal)

	game:GetService("TweenService"):Create(obj, info, goal):Play()

end


local Obj = script.Parent

tween(Obj, TweenInfo.new(1, Enum.EasingStyle.Sine), {Position = UDim2.new(0.5,0,0,320)})

wait(7)

tween(Obj, TweenInfo.new(1, Enum.EasingStyle.Sine), {Position = UDim2.new(0.5,0,0,520)})

wait(1)

script.Parent.Parent.Parent.globalDebounce.Value = false

Obj:Destroy()

