--// Execute the code given on the client as server. Return success if executed
script.Parent.Execute.OnServerEvent:Connect(function(player, scriptObject)
	local loadstring = require(script.Loadstring)
	local status = loadstring(scriptObject)()
	script.Parent.Execute:FireClient(player, "Success!\nScript executed without errors.")
end)
